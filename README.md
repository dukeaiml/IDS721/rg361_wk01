# Static Portfolio Site Using Zola


[![pipeline status](https://gitlab.com/dukeaiml/IDS721/rg361_wk01/badges/main/pipeline.svg)](https://gitlab.com/dukeaiml/IDS721/rg361_wk01/-/commits/main)

## Overview
In this project a static site is generated using [Zola](https://www.getzola.org/) a Static Site Generator (SSG) built on Rust. The base site is further enhanced by using ``HTML`` and ``CSS``. The site is intended to be used as a portfolio site to showcase information such as contact details, projects etc.

The site is hosted on Gitlab and is automatically updated and  deployed whenever any changes are made in this repository. 
 
The site can be accessed by using the following [Link](https://rg361-wk01-dukeaiml-ids721-2284089bf24b6e8f4db8de4fd0ab86194cb8.gitlab.io/).

## Site Content

Currently, the site cotains the following pages:

1. **Home**: This page is the landing page (index page) of the site. It contains a brief introduction about me and links to my various profiles and contact details.

![Home Page](./resources/image_index.png)

2. **Projects**: This page contains a list of projects that I have worked on. Each project is displayed as a card containing the project name, and icons to indicate the expected reading time and number of words. On clicking on the project card, the user is taken to the project page which contains a detailed information of the project.

![Projects Page](./resources/image_projects.png)

3. **Categories**: This page contains a list of categories that the projects are grouped into. On clicking on a category, the user is taken to a page containing a list of projects that belong to that category.

![Categories Page](./resources/image_categories.png)

4. **Tags**: This page contains a list of tags that the projects are grouped into. On clicking on a tag, the user is taken to a page containing a list of projects that belong to that tag.

![Tags Page](./resources/image_tags.png)

5. **Project Pages**: Each project has a dedicated page containing detailed information about the project. The page contains the categories and tags under which the project falls under, relevant links and a detailed description of the project.

![Project Page](./resources/image_project_page.png)


## Site Development Process

1. **Installation of Zola**: The first step was to install Zola. The installation instructions can be found [here](https://www.getzola.org/documentation/getting-started/installation/).

2. **Creation of Repository**: A new repository was created on Gitlab to host the site and it was made into a zola file structure using the command `zola init <project_name>` command.

3. **Base Theme**: A base theme was chosen from the [Zola themes](https://www.getzola.org/themes/) site and the theme was downloaded to the ``themes`` folder. The theme name and the required basic configuration was added to the `config.toml` file.

4. **Modification and Content Creation**: Changes were made to the base template as required using ``HTML``, ``CSS`` and by chnaging the folder structure. content of the site was created using ``markdown`` files.

5. **Testing**: The site was tested locally using the command `zola serve` and the site was accessed at `http://127.0.0.1:1111` in the local machine.

6. **Deployment**: The site was deployed on Gitlab using the `.gitlab-ci.yml` file which was created in the root directory of the repository. This file contains the configuration for the Gitlab CI/CD pipeline which is used to build and deploy the site whenever any changes are made in the repository. the CI/CD pipeline uses the `zola build` command to build the site and the `public` folder is deployed to the Gitlab pages.

## Future Work

Some of the planned future work for the site is:
1. Add more projects and content to the site as and when they are completed.
2. Adding a search bar funtionality to the site.
3. Adding a dynamic backgroud to the site.
